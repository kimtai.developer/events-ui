export const useCounter = () => useState<number>('counter', () => 45)
export const useColor = () => useState<string>('color', () => 'pink')