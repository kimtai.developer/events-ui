// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app:{
    head:{
      title: 'Events - UI',
      link:[
        { href:'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' , rel:'stylesheet' }, 
      ],
      script:[
        { src: 'https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js', type:'text/javascript'},
        { src:'/js/bootstrap.bundle.min.js', type:'text/javascript' },
      ]
    }
  },
  devtools: { enabled: true },
  components:[
    '~/components/',
    '~/components/utils',
    '~/components/common',
  ],
  css: [
    '~/node_modules/bootstrap/dist/css/bootstrap.min.css',
    '~/assets/css/style.css',
    '~/assets/font-awesome/css/font-awesome.min.css'
  ],
  modules: [
    '@pinia/nuxt',
    ['@nuxtjs/google-fonts', {
      families: {
        'DM Sans': [300,400,500,600],
        Inter: [400,500,700],
        Montserrat: [400,500,700],
      }
  }],
  ],
  
})
