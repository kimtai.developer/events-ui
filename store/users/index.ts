import { defineStore } from 'pinia';

interface Todo {
    id: number, 
    completed: boolean, 
    title: string,
    userId: number,
}

export const useUserStore = defineStore({
    id: 'users',
    state: () => ({
        todos: [] as Todo[],
        names:[] as string[],
        count: 0
    }),
    actions: {
        addName( value: string) { 
            this.names.push(value);
        },
        addTodo( value: Todo) { 
            this.todos.push(value);
        },
        addCounter( num: number) {
            this.count += num
        },
        popName() {
            this.names.pop();
        }
    },
    getters: {
        list: state => state.names,
        counter: state => state.count
    }

})